require 'rubygems'
require 'sinatra'
require 'rack'
require 'digest/md5'
require 'sdbm'
require 'sinatra/reloader'
require 'pp'

module Gyazo
  class Controller < Sinatra::Base

    configure do
      set :dbm_path, 'db/id'
      set :image_dir, 'public'
      set :image_url, 'http://gyazo.cignoir.jp'
    end

    get '/' do
      #@images = Dir.glob("public/*.png").map{|s| s.sub(/public\//, '')}
      @images = Dir.glob("public/*.png").map{|s| s.sub(/public\//, '')}.map{|s| s + "@" + File.open("public/" + s).ctime.to_s.sub(/ \+0900/, '')}
      erb :index
    end

    post '/upload' do
      id = request[:id]
      data = request[:imagedata][:tempfile].read
      hash = Digest::MD5.hexdigest(data).to_s
      dbm = SDBM.open(options.dbm_path, 0777)
      dbm[hash] = id
      File.open("#{options.image_dir}/#{hash}.png", 'w'){ |f|
        f.write(data)
        File.chmod(0755, f)
      }
      "#{options.image_url}/#{hash}.png"
    end
  end
end
